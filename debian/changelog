rabbitvcs (0.19-4) unstable; urgency=medium

  * Add superficial autopkgtest
  * Add missing dependency python3-six to rabbitvcs-core
  * Fix the call to 'gio trash' (Closes: #969772)

 -- Dylan Aïssi <daissi@debian.org>  Mon, 10 Feb 2025 22:39:04 +0100

rabbitvcs (0.19-3) unstable; urgency=medium

  [ Alexandre Detiste ]
  * remove dependency on python3-simplejson

  [ Ritesh Raj Sarraf ]
  * Add runtime dependency for thunar and nautilus plugins (Closes: #1065002)
  * Add Dylan Aïssi to Uploaders

  [ Dylan Aïssi ]
  * Remove ipython3 from Build-Deps (Closes: #636471)
  * Switch to pkgconf in Build-Deps
  * Add Rules-Requires-Root: no
  * Standards-Version: 4.7.0 (no changes required)
  * Drop superfluous stanzas in debian/copyright
  * Drop unused paragraphs in debian/copyright


 -- Ritesh Raj Sarraf <rrs@debian.org>  Tue, 10 Sep 2024 15:54:46 +0530

rabbitvcs (0.19-2) unstable; urgency=medium

  * Team upload
  * Source-only upload

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 23 Jul 2023 18:15:49 -0400

rabbitvcs (0.19-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Build-Depend on python3-setuptools
  * Restore rabbitvcs-nautilus package now that the extension
    has been ported to Nautilus >= 43
  * Bump minimum nautilus & python3-nautilus dependencies
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 19 Jul 2023 15:08:55 -0400

rabbitvcs (0.18-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends-Indep: Drop versioned constraint on pkg-config.
    + rabbitvcs-core: Drop versioned constraint on hicolor-icon-theme, ipython3,
      meld and subversion in Depends.
    + rabbitvcs-cli: Drop versioned constraint on rabbitvcs-core in Depends.
    + rabbitvcs-gedit: Drop versioned constraint on rabbitvcs-core in Depends.
    + rabbitvcs-thunar: Drop versioned constraint on rabbitvcs-core in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 28 Oct 2022 20:04:02 +0100

rabbitvcs (0.18-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 02:52:02 +0100

rabbitvcs (0.18-4) unstable; urgency=medium

  * Team upload
  * Drop Nautilus extension since it uses GTK3 but Nautilus 43 uses GTK4

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 01 Sep 2022 08:53:08 -0400

rabbitvcs (0.18-3) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Wed, 15 Sep 2021 23:25:43 -0400

rabbitvcs (0.18-2) unstable; urgency=medium

  * Demote thunarx-python to Recommends (Closes: #955346)

 -- Ritesh Raj Sarraf <rrs@debian.org>  Tue, 31 Mar 2020 21:14:52 +0530

rabbitvcs (0.18-1) unstable; urgency=medium

  * New upstream version 0.18
  * Switch to python 3 (Closes: #938327)
  * Specify pybuild build system
  * Disable auto test invcation, temporarily
  * Drop patch 99_setyup.py.patch
  * Update plugin path for Nautilus
  * Update debian/control

 -- Ritesh Raj Sarraf <rrs@debian.org>  Wed, 25 Mar 2020 19:12:00 +0530

rabbitvcs (0.17.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Ritesh Raj Sarraf ]
  * Fix short description for package rabbitvcs-thunar.
    Thanks to Daniele Forsi (Closes: #935423)
  * Add dependency on python-tk.
    Thanks to Age Bosma (Closes: #939396)

 -- Ritesh Raj Sarraf <rrs@debian.org>  Wed, 04 Sep 2019 19:33:42 +0530

rabbitvcs (0.17.1-2) unstable; urgency=medium

  * Upload to Unstable

 -- Ritesh Raj Sarraf <rrs@debian.org>  Thu, 01 Aug 2019 21:07:28 +0530

rabbitvcs (0.17.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field

  [ Ritesh Raj Sarraf ]
  * New upstream version 0.17.1 (Closes: #882241, #909187)
  * Bump debhelper dependency to 9
  * Add dh-python to build depends
  * Add rabbitvcs-thunar plugin (Closes: #664970)
  * Refresh patch 99_setup.py.patch
  * Drop patch fix-817231.patch. Not relevant anymore
  * Add myself as the maintainer (Closes: #892412)
  * Set upstream site to github hosted one

 -- Ritesh Raj Sarraf <rrs@debian.org>  Thu, 04 Jul 2019 20:37:18 +0530

rabbitvcs (0.16-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "All files in home folder removed" Applied upstream fix as patch (Closes: #817231)

 -- Christopher Hoskin <christopher.hoskin@gmail.com>  Sun, 28 Aug 2016 07:34:26 +0100

rabbitvcs (0.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream release. (Closes: #740530)
  * Update watch file.
  * Bump standards-version to 3.9.6.

 -- Vincent Cheng <vcheng@debian.org>  Sun, 05 Oct 2014 02:12:34 -0700

rabbitvcs (0.15.3-1) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jackson Doak ]
  * New upstream release. Closes: #707775
  * Bump standards-version to 3.9.5

 -- Jackson Doak <noskcaj@ubuntu.com>  Sun, 23 Feb 2014 12:30:43 +1100

rabbitvcs (0.15.2-1) unstable; urgency=low

  * Team upload.
  * New upstream release:
    - Issue 187: Drop-down list of pre-defined properties in the property
      dialog
    - Issue 261: RabbitVCS incompatible with TortoiseHg
    - Issue 562: 'Refresh Status' does not work
    - Issue 585: Fix "Browser can not handle path with non-English name"
    - Issue 586: Fixed "Browser can not handle path with non-English name"
    - Issue 605: Log window Forward button does not work for me
    - Issue 628: (Sub)directories with a white space are not correctly
      managed (closes: #665193)
    - Issue 716: Unicode regression from (Sub)directories with a white
      space are not correctly managed
    - Fixed commit ID not being reported when committing to a git repository.
  * Bump python-svn dependency (upstream issue 564).
  * Make dependency on python-dulwich versioned (upstream issue 579).
  * Drop 50_fix_gedit3_plugin.patch and 60_support_subversion_1.7.patch
    patches which were part of 0.15.1.
  * Update debian/copyright.

 -- Arthur de Jong <adejong@debian.org>  Mon, 19 Nov 2012 23:39:17 +0100

rabbitvcs (0.15.0.5-3) unstable; urgency=low

  * Team upload.
  * Upload to unstable targeted for wheezy.

 -- Arthur de Jong <adejong@debian.org>  Thu, 09 Aug 2012 11:43:56 +0200

rabbitvcs (0.15.0.5-2) experimental; urgency=low

  * Team upload.
  * Add 60_support_subversion_1.7.patch from upstream that adds support
    for Subversion 1.7 working copies.

 -- Arthur de Jong <adejong@debian.org>  Mon, 23 Jul 2012 21:32:41 +0200

rabbitvcs (0.15.0.5-1) unstable; urgency=low

  * Team upload.

  [ Arthur de Jong ]
  * New upstream release (Closes: #618934) (LP: #741562, #952719):
    - Includes new client for Nautilus 3 (Closes: #633113, #644690, #647999)
      (LP: #876514).
    - Fix a problem with rename a file (Closes: #585859).
    - Allows entering repository URL when starting the browser
      (Closes: #619644).
  * Cherry-pick upstream change that implements a gedit 3 plugin and update
    packaging according to new directory layout (closes: #635123).
  * Update package descriptions to match upstream's (Closes: #580716)
    (LP: #748304).
  * Drop dependency on glade (not used in 0.15).
  * Add missing dependency on python-simplejson (LP: #769742)
  * Have rabbitvcs-nautilus Conflicts/Replaces rabbitvcs-nautilus3 to
    ensure smooth upgrades for people running upstream provided packages.
  * Update debian/copyright and switch to latest machine-readable format.
  * Move packaging to Python Applications Packaging Team repository,
    updating maintainer and Vcs-* fields.
  * Switch to using dh_python2.
  * Change package section to vcs (Closes: #626809).
  * Bump Standards-Version to 3.9.3.

  [ Jakub Wilk ]
  * Add Vcs-* fields.

  [ Sameer Rahmani ]
  * watch file fixed.

 -- Arthur de Jong <adejong@debian.org>  Wed, 21 Mar 2012 23:09:27 +0100

rabbitvcs (0.13.1-2) unstable; urgency=low

  * Team upload
  * debian/control
    - added python-gnome2 to Depends of rabbitvcs-nautilus; Closes: #610473

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Thu, 20 Jan 2011 00:50:44 +0100

rabbitvcs (0.13.1-1) unstable; urgency=low

  * Replaced multiple tarballs with single tarball plus split-out binary
    packages.
  * Again closes: #469181.

 -- Jason Heeris <jason.heeris@gmail.com>  Fri, 02 Apr 2010 17:03:15 +0800
